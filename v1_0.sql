-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: localhost    Database: sp_pr
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `userId` int NOT NULL AUTO_INCREMENT,
  `addressLine1` varchar(60) DEFAULT NULL,
  `addressLine2` varchar(60) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `contactNumber` varchar(14) DEFAULT NULL,
  `disableUser` tinyint(1) NOT NULL DEFAULT '1',
  `email` varchar(20) DEFAULT NULL,
  `name` varchar(18) DEFAULT NULL,
  `password` varchar(12) DEFAULT NULL,
  `pincode` int DEFAULT NULL,
  `state` varchar(15) DEFAULT NULL,
  `typeId` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `UK_e6gkqunxajvyxl5uctpl2vl2p` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (2,'karve nagar','NULL STOPT','pune','8498787890',0,'prasant@gmail.com',NULL,'123',711037,'MH','CUSTOMER'),(3,'magarpatta','IT circle magrapatta','pune','9498787890',0,'snehal@gmail.com',NULL,'123',711027,'MH','CUSTOMER'),(4,'Hinjewadi','phase 3','pune','9448787890',0,'sid@gmail.com',NULL,'123',711028,'MH','ADMIN'),(5,'wakad','hinjewadi phase 1','Pune','884934253',0,'mohit@gmail.com','mohit','1234',411029,'M.H.','ADMIN'),(7,'pashan','pashan chowk','pune','8830134498',0,'paras@gmail.com','paras','1234',411031,'M.H.','CUSTOMER');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicle`
--

DROP TABLE IF EXISTS `vehicle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vehicle` (
  `vId` int NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `chessisNumber` varchar(20) DEFAULT NULL,
  `modelYear` int DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`vId`),
  UNIQUE KEY `UK_em4qf11xjtkjbyrxtlolavxuo` (`chessisNumber`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicle`
--

LOCK TABLES `vehicle` WRITE;
/*!40000 ALTER TABLE `vehicle` DISABLE KEYS */;
INSERT INTO `vehicle` VALUES (1,1,'ABXX122',2004,'SEDAN'),(2,1,'ABCXX122',2006,'CROSSOVER'),(3,1,'ABCXX132',2008,'MUV'),(4,1,'ABCXXYY2',2000,'SUV');
/*!40000 ALTER TABLE `vehicle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehiclebooking`
--

DROP TABLE IF EXISTS `vehiclebooking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vehiclebooking` (
  `bId` int NOT NULL AUTO_INCREMENT,
  `fromPlace` varchar(30) DEFAULT NULL,
  `toPlace` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`bId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehiclebooking`
--

LOCK TABLES `vehiclebooking` WRITE;
/*!40000 ALTER TABLE `vehiclebooking` DISABLE KEYS */;
INSERT INTO `vehiclebooking` VALUES (1,'Bhopal','pune'),(2,'yavatmal','amravati'),(3,'kandivali','andheri'),(4,'andheri','thane'),(5,'',''),(6,'majestic','marathalli');
/*!40000 ALTER TABLE `vehiclebooking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicleusage`
--

DROP TABLE IF EXISTS `vehicleusage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vehicleusage` (
  `usId` int NOT NULL AUTO_INCREMENT,
  `total` int DEFAULT NULL,
  `unitCharge` int DEFAULT NULL,
  `usageUnits` int DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`usId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicleusage`
--

LOCK TABLES `vehicleusage` WRITE;
/*!40000 ALTER TABLE `vehicleusage` DISABLE KEYS */;
INSERT INTO `vehicleusage` VALUES (6,54,18,3,'available'),(7,76,19,4,'available'),(8,63,21,3,'available'),(9,72,24,3,'available'),(10,0,30,2,NULL);
/*!40000 ALTER TABLE `vehicleusage` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-29 20:56:21
