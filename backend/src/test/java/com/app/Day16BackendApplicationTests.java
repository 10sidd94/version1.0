package com.app;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.app.dao.UserRepository;
import com.app.pojos.User;
import com.app.pojos.UserTypeId;
import com.app.service.IUserService;

@SpringBootTest
class Day16BackendApplicationTests {
	@Autowired
	private IUserService userService;
	@Autowired
	private UserRepository userRepo;
	
	@Test
	void contextLoads() {
		List<User> allUsers = userService.getAllUsers();
		System.out.println(allUsers);
		assertEquals(2, allUsers.size());
	}
	//add a test case to insert user data
	@Test
	public void testSaveUser() throws Exception
	{
		User user=new User(2, "Snehal", "Snehal@gmail.com", "123","28283434", UserTypeId.Admin,"Karve nagar","near Null Stop", "pune",411030, "M.H.", false);
		User persistentUser = userRepo.save(user);
		assertEquals(2, persistentUser.getUserId());
	}

}
