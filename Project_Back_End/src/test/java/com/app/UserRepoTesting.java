package com.app;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.app.dao.UserRepository;
import com.app.pojos.User;
import com.app.service.IUserService;



class UserRepoTesting {
	
	@Autowired
	private IUserService userService;

	@Autowired
	private UserRepository userRepo;
	

	@Test
	void contextLoads() {
		List<User> allUsers =  userService.getAllUsers();
		System.out.println(allUsers);
		assertEquals(0, allUsers.size());
	}
//	@Test
//	void testAddCustomer() {
//		User user=new User(1, "Mihir", "Mihir@gmail.com", "123","28283434", UserTypeId.Admin,"Bopodi road","near old dapodi road", "pune",4700001, "M.H.", false);
//		User Saveduser = userRepo.save(user);
//		assertEquals("1", Saveduser.getUserId());
////		Customer c2=new Customer("hdfc-1002","Kiran","kir#123");
////		Customer customer2 = custRepo.save(c2);
////		assertEquals("hdfc-1002", customer2.getCustomerId());
//	}

}
///users