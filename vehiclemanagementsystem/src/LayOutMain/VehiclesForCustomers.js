
import React, { Component } from 'react';
import VehicleService from './VehicleService';
import { Link } from "react-router-dom";
import { useEffect } from "react";

class ListVehicleForCustomerComponent extends Component {

    constructor(props) {
        console.log("Vehicle constructor");

        super(props)

        this.state = {
            vehicle: []
        }


        // useEffect(() => {
        //     loadUsers();
        //   }, []);


        // this.addEmployee = this.addEmployee.bind(this);
        // this.editEmployee = this.editEmployee.bind(this);
        // this.deleteEmployee = this.deleteEmployee.bind(this);
    }
    //change delete with glyficon or google icon
    // deleteUser(email){
    //     VehicleService.Service.deleteUser(email).then( res => {
    //     this.setState({user: this.state.user.filter(user=> user.email !== email)});
    //     });
    // }
    // viewUser(email){
    //     console.log("User emailid=" +email);
    //     //no need of link in hstory.push
    //     this.props.history.push('/ViewUserComponent');

    // }
    //  loadUsers = async () => {
    //     const result = await axios.get("http://localhost:9898/api/users");
    //     setUser(result.data.reverse());
    //   };


    // editEmployee(id){
    //     console.log("user id=" +id);
    //     this.props.history.push(`add-user/${id}`)
    // }

    componentDidMount() {
        VehicleService.getVehicles().then((res) => {
            this.setState({ vehicle: res.data });
            console.log(this.state.vehicle[0])
        });
    }

    // addUser(){
    //     console.log("Add new user" );
    // }
    render() {
        return (
            <div>
                <h2 className="text-center">Book Vehicle </h2>
                {/* <div className = "row">
                    <Link class="btn btn-outline-primary mr-2" to={`/user/adduser`} >Add Vehicle </Link>
                 </div> */}
                <br></br>
                <div className="row">
                    <table className="table table-striped table-bordered">

                        <thead>
                            <tr>
                                {/* <th><option><select/></option></th> */}
                                <th> Vehicle Id</th>
                                <th> Vehicle type</th>
                                <th> Vehicle chessisNumber</th>
                                <th> Whether Active</th>
                                <th> Vehicle Year of Model</th>

                                <th> Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.vehicle.map(
                                    vehicle =>

                                        <tr key={vehicle.vId}>
                                            <td>{vehicle.vId}</td>
                                            <td> {vehicle.type} </td>
                                            <td> {vehicle.chessisNumber} </td>
                                            <td> {vehicle.active.toString()} </td>
                                            <td> {vehicle.modelYear}</td>
                                            <td>
                                                <Link className="btn btn-primary" to={`/vehicle/vehicleId`}>Book</Link>

                                            </td>
                                        </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>

            </div>
        )
    }


}

export default ListVehicleForCustomerComponent;