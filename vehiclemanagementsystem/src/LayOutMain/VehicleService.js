import axios from 'axios'

const USERS_API_BASE_URL="http://localhost:8080/spring_backend/vehicle"

// axios.defaults.headers.common = {
//     ...axios.defaults.headers.common,
//     'Access-Control-Allow-Origin': 'http://localhost:8080/day15_spring_mvc/user',
//     "Content-Type": 'application/json'
//  };
//  axios.defaults.preflightContinue = true;
//  //axios.defaults.crossDomain = true;

class UserService
{

    
    getVehicles()
    {
        return axios.get(USERS_API_BASE_URL+'/vList')
    }

    getUserById(email)
    {
        return axios.get(USERS_API_BASE_URL+'/'+email)
    }
    
    deleteUser(email)
    {
        return axios.delete(USERS_API_BASE_URL+'/'+email)
    }

    updateUser(email)
    {
        return axios.put(USERS_API_BASE_URL+'/'+email)
    }
}

export default new UserService()