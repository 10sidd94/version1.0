import React, { useState } from "react";
import axios from 'axios'
import { useHistory,useParams } from "react-router-dom";

const BookVehicleUsage = () => {
  let history = useHistory();
  const { vId } = useParams();
  const [vehicleUsage, setVehicleUsage] = useState({
    userId: "",  
    vehicleId:vId,
    unitCharge:"",
    usageUnits:"",
    total:""
  });

  const {  userId,vehicleId,status,unitCharge,usageUnits,total } = vehicleUsage;
  const onInputChange = e => {
    setVehicleUsage({ ...vehicleUsage,[e.target.name]:e.target.value });
  };

  const onSubmit = async e => {
    e.preventDefault();
    console.log(vehicleUsage)
    await axios.post("http://localhost:8080/spring_backend/vehicleUsage/addUsage",vehicleUsage);
    history.push("/pdfGeneraterForVehicleIvoice.js");
  };
  return (
    <div className="container">
      <div className="w-75 mx-auto shadow p-5">
        <h2 className="text-center mb-4">Payables</h2>
        <form onSubmit={e => onSubmit(e)}>

       

          <div className="form-group">
            <input
              type="number"
              className="form-control form-control-lg"
              placeholder="unitCharge"
              name="unitCharge"
              
              onChange={e => onInputChange(e)}
            />
          </div>
          <div className="form-group">
            <input
              type="String"
              className="form-control form-control-lg"
              placeholder="Enter ,no. of hour"
              name="usageUnits"
              onChange={e => onInputChange(e)}
            />
          </div>

          <div className="form-group">
            <input
              type="number"
              className="form-control form-control-lg"
              placeholder="total payable"
              name="total"
              value={unitCharge*usageUnits}
              onChange={e => onInputChange(e)}
            />
          </div>

         

         
          
          {/* <Link className="btn btn-primary" to={`/vehicle/vehicleUsage/${vehicle.vehicleId}`}>PayOut</Link> */}
        
          <button className="btn btn-primary btn-block"> press button payout with bill in pdf </button>
        </form>
      </div>
    </div>
  );
};

export default BookVehicleUsage